
# changelog getFirefox

tags utilisés: added  changed  cosmetic  deprecated  fixed  rewriting  removed  syncro  security
date au format: YYYY-MM-DD

## [ Unreleased ]


## [ 4.29.1 ] - 2019.07.08

* cosmetic: ffx_get_version
* fixed: f__wget_test
* syncro: fscript_install, fscript_remove, fscript_update, f__user

## [ 4.28.0 ] - 2019.07.06

* rewriting f__wget_test
* rewriting: récup versions on ligne: ffx_get_version, ffx_pers_get_version

## [ 4.27.2 ] - 2019.05.4

* fixed: bash v5 utilisable (v4+)

## [ 4.26.0 ] - 2018.07.3

* synchro: f__requis, f__sort_uniq

## [ 4.25.0 ] - 2018.06.26

* fixed: typo, lancement upgrade

## [ 4.24.2 ] - 2018.06.13

* added: f__read_file
* rewriting: suppression sed peu utiles
* rewriting: f__archive_test
* cosmetic: exit sur --help

## [ 4.24.1 ] - 2018.06.12

* syncro: composants, fscript_update avec ses paramètres d'appel
* added: option --dev pour chargement version dev
* added: option téléchargement
* rewriting: traitement options d'appel
* rewriting: ffx_get_version, prise en charge 2 versions Esr
* rewriting: f__sudo, plus de multiples tentatives pour su, plus simple et fix potentiel stderr non visible
* fixed: nouvelle version esr possible
* fixed: création répertoire temporaire téléchargement 

## [ 4.21.2 ] - 2018.06.09

* cosmetic: personnalisation en ligne, f__architecture
* fixed: bug mineur à la mise à jour, si pas de personnalisation installée

## [ 4.21.1 ] - 2018.06.07

* fixed: renommage éventuel anciens fichiers de personnalisation

## [ 4.21.1 ] - 2018.06.07

* fixed: url sur sdeb, supp option test oubliée

## [ 4.21.0 ] - 2018.06.07

* fixed: lanceur desktop potentiellement malformé 
* cosmetic: shellcheck, affichage ascii & help
* added: f__trim
* rewriting: suppression utilisation xargs, utilisations f__trim (pas de sous-shell et plus rapide)
* rewriting: ffx_pers_get_version, détermination url personnalisée des personnalisations
* fixed: mise à jour url de personnalisation sur nouveau git

## [ 4.20.1 ] - 2018.06.06

* rewriting: f__requis (shellcheck)

###  publication sur sdeb/getFirefox, historique abandonné

## [ 4.19.0 ] - 2018.03.09

* fixed: droits 

## [ 4.19.0 ] - 2018.03.09

* rewriting: shellcheck
* cosmetic: revue traitement options
* rewriting: f_affichage
* syncro: f__color f__info f__log f__requis f__sudo f__user 
* syncro: fscript_cronAnacron fscript_get_version fscript_update
* fixed: ffx_lanceur_desktop, plusieurs versions simultanées possibles, réparer avec `getFirefox ri`

## [ 4.18.0 ] - 2018.03.04

* rewriting:

## [ 4.17.1 ] - 2018.03.04

* syncro: fscript_install, fscript_remove, fscript_update
* syncro: f__color, f__info, f__sudo, f__user, f__wget_test
* rewriting: prg_init, f_help
* rewriting: général ubuntu 16.04, ffx_get_version, ffx_install, ffx_pers_get_version

## [ 4.16.1 ] - 2018.02.11

* syncro: f__color

## [ 4.16.0 ] - 2018.02.01

* rewriting: +requis awk>gawk
* fixed: ffx_config_system, alternatives si primo installation avec aucune alternative existante

## [ 4.15.0 ] - 2018.01.26

* rewriting: mineur, fscript_cronAnacron fscript_install fscript_remove fscript_update
* rewriting: f__requis
* fixed: f__sudo, extraction nb tentatives

## [ 4.13.1 ] - 2018.01.25

* rewriting: f_help, f_affichage
* rewriting: ffx_install
* rewriting: général wget_log: fscript_get_version, fscript_update

## [ 4.12.0 ] - 2018.01.16

* added: cumul options (opérations) possibles (sauf opés scripts)
* added: option --sauve pour conserver le téléchargement à l'installation
* rewriting: ffx_lanceur_desktop, mimetype, plus de xul, pas de déclaration images
* rewriting: f__wget_test
* rewriting: f_sudo abandonné dans fscript_install et fscript_remove, au profit appel au traitement général des options
* rewriting: général, invocation f__sudo dans traitement options, plus confortable si su & _all_
* rewriting: f_sudo, format nombre de tentatives et options appel possibles > 1
* rewriting: $appli en global au lieu de local
* rewriting: auto-installation, potentiel bug selon conditions appel
* rewriting: réparation icône (endroit du test root pour option all, une seule saisie pw pour tous les canaux)
* fixed: test gnome-www-browser avant installation (erreur kde) 
* fixed: inscription Default dans profil.ini (synchro fix getThunderbird oubliée :/)

## [ 4.11.0 ] - 2018.01.12

* rewriting: menu réparation icône   
  * all pour tous les canaux installés   
  * menu help   
* fixed: correction commentaire fscript_get_version
* fixed: correction condition auto-install

## [ 4.10.0 ] - 2017.12.31

* added: menu réparation icone pour pallier au changement lors mise à jour Fx

## [ 4.9.0 ] - 2017.12.29

* syncro: composants

## [ 4.8.1 ] - 2017.12.27

* rewriting: ffx_config_system, meilleur parsage pour default=1 en dernière position

## [ 4.7.0 ] - 2017.12.26

* rewriting: f__info, option combinée raw:log
* rewriting: f_help
* rewriting: config_profil, config_system (traitement profil et défaut système) 
* rewriting: ffx_install
* fixed: ffx_remove, oubli suppression lien dans /usr/bin

## [ 4.5.0 ] - 2017.12.25

* cosmetic:
* rewriting: ffx_install, ffx_profil_user
* rewriting: général, plus de variables composées avec variable dans local
* rewriting: ffx_lanceur_desktop changement en cours ou bug mozilla sur icon 
* fixed: ffx_get_version, affichage

## [ 4.3.0 ] - 2017.12.24

* cosmetic:
* fixed: f__wget_test, incompatible avec redirection logs
* fixed: typo fscript_update
* fixed: typo ffx_pers_install

## [ 4.1.0 ] - 2017.12.23

* rewriting: général, tous les wget, définition logs, pour cause de bug wget? sur testing
* rewriting: général, tous les wget, répertoires temp fixes (plus de random) pour permettre reprise DL
* fixed: mineur, f_help
* fixed: ffx_install, appel sudo

## [ 4.0.0 ] - 2017.12.22

* changed: remodelage complet
* changed: Firefox only, script scindé pour waterfox, torbrowser

## [ 3.16.0 ] - 2017.12.21

* rewriting: update alternatives
* fixed: mineurs

## [ 3.15.0 ] - 2017.12.18

* fixed: install manuelle, bug potentiel

## [ 3.14.1 ] - 2017.12.15

* syncro: f__architecture

## [ 3.14.0 ] - 2017.12.13

* rewriting: f__wget_test

## [ 3.13.0 ] - 2017.12.6

* rewriting: fscript_update, controle chargement début et fin
* rewriting: changement séquence start pour éviter erreur cron

## [ 3.12.1 ] - 2017.12.5

* rewriting: démarrage
* syncro: fonctions communes
* rewriting: suppression avertissement ESR, compatibilité
* rewriting: f_tor_install, ffx_install, renommage $architecture
* rewriting: f_tor_install, f_tor_lanceur_desktop, f_tor_pers_install, ffx_install, 
		ffx_default, ffx_install, ffx_pers_install, ffx_profil_user, fscript_cronAnacron_special,
		renommage $user_
* rewriting: fscript_cronAnacron, fscript_install, fscript_remove, fscript_update 
		f__log, renommage  $fileInstall $fileLogs
* rewriting: affichage ffx_get_version, ffx_pers_get_version
* fixed: f__wget_test

## [ 3.11.0 ] - 2017.10.16

* rewriting: f__error f__info f__requis f__wget_test 

## [ 3.10.0 ] - 2017.10.11

* fixed: f__sudo : fonctionnement avec sudo

## [ 3.9.0 ] - 2017.10.08

* added: f__wget_test(): option test, nommage fichier temp
* added: f__user, premier essai root only, fonctionnement en root only en console
* added: test bash4 au démarrage
* rewriting: f__color: utilisation terminfo pour retour au std (et non noir)
* rewriting: fscript_get_version fscript_install fscript_remove fscript_update
* changed: intégration f__sudo dans install & remove script

## [ 3.8.2 ] - 2017.09.24

* fixed: mineur détection pb 3, raccourci /usr/bin

## [ 3.8.0 ] - 2017.09.22

* cosmetic: fonctions tor
* fixed: correction lanceurs desktop
* cosmetic: ffx_get_versionFF en ffx_get_version, présentation
* syncro: ffx_get_version, ffx_pers_get_version, ffx_pers_upgrade, ffx_get_canalBas	unset/for, f__requis, f__info, f__error	unset/for

## [ 3.7.0 ] - 2017.09.09

* removed: ligne test dev
* présentation ffx_get_versionsFF
* fixed: f_help
* rewriting: f_tb_install, f_tb_install 
* rewriting: ffx_profil_user, suppression ffx_profilIni_inscription inutile en fonction()
* rewriting: f__wget_test, fscript_get_version, f__log
* added: option all pour remove
* changed: gestion paquet firefox-esr en place
* fixed: bug f_tb_remove, ffx_remove
* added: ffx_default (copie default sur un canal)

## [ 3.6.2 ] - 2017.09.06

* rewriting: présentation version, espaces au lieu de Tabs
* fixed: droits sur personnalisation (root sur version dev?) ffx_pers_install
* fixed: préventif sur f_tb_pers_install
* cosmetic: dirInstallTB="/opt/usr/share/tor-browser" au lieu de "/opt/usr/share/tor-browser/"
* cosmetic: f_tb_get_version, f_tb_install, f_tb_lanceur_desktop, f_tb_pers_get_version, f_tb_pers_install, 
* cosmetic: f_tb_pers_remove, f_tb_remove
* cosmetic: profilTor
* cosmetic: f_tb_pers_get_version, f_tb_pers_install
* cosmetic: dirTemp
* cosmetic: f_tb_install, ffx_install, ffx_pers_install
* cosmetic: user_
* cosmetic: ffx_profil_user
* cosmetic: fscript_cronAnacron, fscript_update, fscript_install, fscript_remove
* rewriting: f_tb_get_version, présentation si non installé
* rewriting: f_tb_pers_upgrade, ffx_pers_upgrade, pas de log si pas de maj

## [ 3.4.0 ] - 2017.09.05

* rewriting: ffx_pers_upgrade, f_tb_pers_upgrade pour éviter message au cron

## [ 3.3.0 ] - 2017.09.04

* syncro: ffx_pers_install, fscript_install, fscript_update (return)
* added: IFS

## [ 3.2.0 ] - 2017.09.03

* rewriting: appel fscript_remove, fscript_install & fscript_update
* fixed: mineur ffx_pers_install si utilisation en root 

## [ 3.1.0 ] - 2017.09.02

* rewriting: mineure f_tb_get_version, f_tb_pers_install, ffx_pers_install
* fixed: url personnalisée pour ffx_pers_get_version
* added: option suppression des personnalisations

## [ 3.0.0 ] - 2017.09.01

* rewriting: f__wget_test
* added: tor-browser

## [ 2.9.2 ] - 2017.08.31

* fixed: ffx_install, message installation manuelle

## [ 2.9.1 ] - 2017.08.30

* rewriting: f__requis, f__user, f__wget_test, fscript_cronAnacron
* rewriting: déclaration local
* rewriting: f__archive_test, ffx_install_manuel
* rewriting: affichage ffx_get_versionsFF, ffx_pers_get_version, ffx_pers_install
* rewriting: si produit all, installation personnalisation uniquement pour les produits installés au lieu de f__error
* fixed: version, tb anticipé

## [ 2.8.0 ] - 2017.08.30

* rewriting: conditions d'utilisations, fscript_install, fscript_remove(), fscript_update
* rewriting: appel, fscript_install, fscript_remove(), fscript_update
* cosmetic: f_help 

## [ 2.7.0 ] - 2017.08.28

* fixed: localisation fileDev
* rewriting: f__wget_test
* rewriting: ffx_pers_get_version, ffx_get_versionsFF

## [ 2.6.0 ] - 2017.08.27

* fixed: install
* syncro: fscript_cronAnacron, fscript_install : changement lognameDev ->fileDev
* rewriting: présentation fscript_get_version, fscript_install, fscript_remove

## [ 2.5.0 ] - 2017.08.26

* cosmetic: fscript_dl en fscript_update
* rewriting: fscript_install, fscript_update, fscript_get_version
* rewriting: f__wget_test
* rewriting: f__error, f__info
* added: option personnalisation
* changed: fscript_install pour éventuel fscript_install_special
* added: canal **all** pour install et personnalisation
* changed: maj affichage versions personnalisation
* fixed: upgrade

## [ 2.2.1 ] - 2017.08.23

* changedment: délais anacron, fscript_cronAnacron

## [ 2.2.0 ] - 2017.08.22

* syncro: fscript_dl
* fixed: ffx_profil_user, opérations nécessitant root sans intérêt pour u-canal, root non nécessaire

## [ 2.1.0 ] - 2017.08.21

* rewriting: f__user
* rewriting: log, pas de maj script
* syncro: fscript_dl, fscript_install, fscript_remove, f__info

## [ 2.0.9 ] - 2017.08.20

* fixed: fscript_cronAnacron appel fscript_cronAnacron_special
* fixed: $TERM
* rewriting: fscript_cronAnacron, plus de redémarrage service cron inutile & fonction spécifique pour certains scripts
* rewriting: fscript_dl plus de sortie progression download
* rewriting: fscript_get_version inclut version en cours
* rewriting: fscript_install mise en page
* added: install archive manuelle
* rewriting: ffx_install
* added: plusieurs options lancement possibles

## [ 2.0.6 ] - 2017.08.18

* syncro: fscript_cronAnacron lors upgrade et spécial pour dev 
* syncro: fscript_get_version, fscript_dl, fscript_install
* added: vérification requis pour fonctionnement script

## [ 2.0.5 ] - 2017.08.17

* added: test inscription crontab pour recherche bug siduction
* added: test crontab et modif anacrontab lors upgrade

## [ 2.0.2 ] - 2017.08.16

* changedment: homogénéisation des options d'appel du script
* rewriting: homogénéisation des fonctions de script , localisation anacrontab 
  **réinstallation manuelle nécessaire (root)**
* rewriting: chmod
* rewriting: lancement cron restart (bien que inutile?) à l'installation et à la suppression du script
* fixed: firefox -P non fonctionnel
* fixed: critique: maj non fonctionnelle avec fonction f__user récente.. :(
* rewriting: f__user
* rewriting: f__requis

## [ 1.6.0 ] - 2017.07.31

* fixed: détection user sous gnome

## [ 1.5.2 ] - 2017.07.31

* revue log upgrade
* added: option version script

## [ 1.5.1 ] - 2017.07.30

* cosmetic:
* fixed: pas d'inscription superflue dans crontab si plusieurs installations scripts successives

## [ 1.4.2 ] - 2017.07.28

* changed: fichier log spécifique mais nécessite réinstallation en root (pas de mise à jour)
* changed: logs d'évènements majeurs
* fixed: suppression ancienne localisation /usr/local/bin/
* added: avertissement maj manuelle à faire pour nouveaux logs 

## [ 1.3.1 ] - 2017.07.27

* supprimé: possibilité installation en user si préexistant
* changed: détection paquet installé
* fixed: lanceur desktop 
* rewriting: grep quiet

## [ 1.2.1 ] - 2017.07.27

* fixed: période anacron

## [ 1.2.0 ] - 2017.07.26

* dépendance killall
* replacement lien script dans /usr/bin/ (meilleure compat?)
* cosmetic:
* changed: plus de désinstallation automatique firefox-esr, avertissement

## [ 1.1.0 ] - 2017.07.24

* changed: remove, cas de figure plus aucun canal installé fixé
* fixed: correction help

## [ 1.0.0 ] - 2017.07.24

* 1ère publication
