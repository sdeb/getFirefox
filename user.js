/*v0.6*/

// 06/09/2017

// ! https://developer.mozilla.org/fr/docs/Mozilla/Preferences/A_brief_guide_to_Mozilla_preferences
// http://kb.mozillazine.org/About:config_entries
//http://www.tweakguides.com/Firefox_1.html

////////////////////////////////////
//**** configuration de base ****//

//désactive gestion auto cache
//user_pref("browser.cache.disk.smart_size.enabled", false);
//fixer valeur cache
//user_pref("browser.cache.disk.capacity", 256000);

// bloque autorefresh ou redirection, demande pour accepter
user_pref("accessibility.blockautorefresh", true);

//télémetrie, par défaut true
user_pref("datareporting.healthreport.uploadEnabled", true);
// rapport de plantages, par défaut: false
user_pref("browser.crashReports.unsubmittedCheck.autoSubmit", true);

//pas de newtabpage (page blanche) mais toujours saloperie engrenage pour activer facilement :(
user_pref("browser.newtabpage.enabled", false);
// pas de sites suggérés
user_pref("browser.newtabpage.enhanced", false);
// sources des sites suggérés 
//ini: user_pref("browser.newtabpage.directory.source", "https://tiles.services.mozilla.com/v3/links/fetch/%LOCALE%/%CHANNEL%");
user_pref("browser.newtabpage.directory.source", "");
user_pref("browser.newtab.preload", false);

//ctrl+tab "chronologique"
user_pref("browser.ctrlTab.previews", true);

// dernière session
user_pref("browser.startup.page", 3);

// pas de suggestion dans la barre d'adresses
user_pref("browser.urlbar.searchSuggestionsChoice", false);
user_pref("browser.urlbar.suggest.openpage", false);
user_pref("browser.urlbar.suggest.searches", false);

// pas de first run
user_pref("extensions.shield-recipe-client.first_run", false);

// font sans-serif par défaut
user_pref("font.default.x-western", "sans-serif");

// paramètre historique personnalisé pour gestion cookie tiers
user_pref("privacy.history.custom", true);
// 3: cookies tiers depuis le site visité, conserver jusqu'à la fermeture firefox
user_pref("network.cookie.cookieBehavior", 3);
user_pref("network.cookie.lifetimePolicy", 2);

// nettoyage du cache du prédicteur de liens?
user_pref("network.predictor.cleaned-up", true);

// ne prévient pas de la fermeture d'onglets
user_pref("browser.tabs.warnOnClose", false);

// pas d'avertissement sur ouverture plusieurs onglets
user_pref("browser.tabs.warnOnOpen", false);

// personnalisation, +simple et pas de pocket, abandon, danger pour future évolution de plus en plus merdique?
// pour l'affichage des toolbars, menu ou personal (bookmarks), xulstore.json remplace localstore.rf mais ne 
// être scripté facilement, avant premier démarrage par exemple.
// browser.uiCustomization.state remplace ou sera remplacé par xulstore.json
// user_pref("browser.uiCustomization.state", "{\"placements\":{\"PanelUI-contents\":[\"edit-controls\",\"zoom-controls\",\"new-window-button\",\"privatebrowsing-button\",\"save-page-button\",\"print-button\",\"history-panelmenu\",\"fullscreen-button\",\"find-button\",\"preferences-button\",\"add-ons-button\",\"developer-button\",\"sync-button\"],\"addon-bar\":[\"addonbar-closebutton\",\"status-bar\"],\"PersonalToolbar\":[\"personal-bookmarks\"],\"nav-bar\":[\"urlbar-container\",\"search-container\",\"bookmarks-menu-button\",\"downloads-button\",\"home-button\",\"screenshots_mozilla_org-browser-action\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"toolbar-menubar\":[\"menubar-items\"]},\"seen\":[\"pocket-button\",\"developer-button\",\"screenshots_mozilla_org-browser-action\",\"webide-button\"],\"dirtyAreaCache\":[\"PersonalToolbar\",\"nav-bar\",\"TabsToolbar\",\"toolbar-menubar\",\"PanelUI-contents\",\"addon-bar\"],\"currentVersion\":6,\"newElementCount\":0}");

// pas d'avertissement sur about:config
user_pref("general.warnOnAboutConfig", false);

// par défaut:
user_pref("intl.accept_languages", "fr, fr-fr, en-us, en");
user_pref("general.useragent.locale", "fr");

// Use LANG environment variable to choose locale, par défaut false, utilisation config FF 
//pref("intl.locale.matchOS", true);

///////////////////////////////
//**** personnalisation ****//

// tour, visite guidée de + en + intrusive, on désactive
user_pref("browser.onboarding.enabled", false);
user_pref("browser.onboarding.hidden", true);
user_pref("browser.onboarding.notification.finished", true);
// désactiver uitour
user_pref("browser.uitour.enabled", false);

// 2 sauvegardes de bookmarks (au ieu de 15)
user_pref("browser.bookmarks.max_backups", 2);

// pas de fermeture fenêtre lors fermeture dernier onglet
user_pref("browser.tabs.closeWindowWithLastTab", false);

//stop démarrage auto des lecteurs media, peut-être limité aux vidéos HTML5
user_pref("media.autoplay.enabled", false);

//nb d'onglet pouvant être réouverts, standard 10
user_pref("browser.sessionstore.max_tabs_undo", 10);
//nb de fenêtres pouvant être réouvertes, standard 3
user_pref("browser.sessionstore.max_windows_undo", 10);

// activation protection active pistage
user_pref("privacy.trackingprotection.enabled", true);
// pour nightly pour l'instant, icone permanente pour on/off
user_pref("privacy.trackingprotection.ui.enabled", true);

// fix choix, message lors blocage popup
user_pref("privacy.popups.showBrowserMessage", true);

// fix windows, pas d'action pour backspace
user_pref("browser.backspace_action", 2);

// liens visités, https://bugzilla.mozilla.org/show_bug.cgi?id=385111, http://htmlcolorcodes.com/fr/
// ne force pas sur les choix déplorables en terme de contraste de certains sites
// std: user_pref("browser.visited_color", "#551A8B"); bleu foncé
// clair: pref("browser.visited_color", "#CC33CC"); Medium Faded Magenta
// clair: pref("browser.visited_color", "#CC00CC"); Dark Hard Magenta
// purple (foncé): pref("browser.visited_color", "#800080");
// Dark Dull Magenta, plus clair: 
user_pref("browser.visited_color", "#993399");

//force punycode pour idn anti_phishing, non résolu depuis début 2017!
user_pref("network.IDN_show_punycode", true);

// Override default layout.css.dpi value
// -1 Use the host system's logical resolution or 96, whichever is greater, for interpreting dimensions specified in absolute units. (Default)
// 0 Use the host system's logical resolution for interpreting dimensions specified in absolute units.
// >= 1 (eg. 96, 120) Use this number for interpreting dimensions specified in absolute units.
user_pref("layout.css.dpi", 0);

// pas de masquage de http:// (https:// pas concerné) et du slash final
user_pref("browser.urlbar.trimURLs", false);

//si true, sélectionne tout sur un clic dans la barre d'adresse 
user_pref("browser.urlbar.clickSelectsAll", true);

//maxi popups ouvertes sur un clic
user_pref("dom.popup_maximum", 5);

//geolocation activé, en standard true
user_pref("geo.enabled", true);

//javascript activé, en standard true
user_pref("javascript.enabled", true);

// soumettre les url invalides au moteur de recherches, std true
user_pref("keyword.enabled", true);

// deviner les url sur un mot, std true, utile si keyword désactivé
user_pref("browser.fixup.alternate.enabled", false);

// 0: ne jamais activer, 1: demander pour activer, 2: toujours activer
user_pref("plugin.state.java", 0);
user_pref("plugin.state.flash", 1);
// when Flash enabled, download and use Mozilla SWF URIs blocklist 
user_pref("browser.safebrowsing.blockedURIs.enabled", true);

// enable hardening against various fingerprinting vectors (Tor Uplift project)
// https://wiki.mozilla.org/Security/Tor_Uplift/Tracking
user_pref("privacy.resistFingerprinting", true);

//désactiver getpocket saloperie commerciale rachetée par mozilla corp
user_pref("extensions.pocket.enabled", false);

// désactiver la manipulation du click droit/menu contextuel par js
user_pref("dom.event.contextmenu.enabled", false);
// désactiver la manipulation du presse papier par js
user_pref("dom.event.clipboardevents.enabled", false);
// désactiver resize et move par js 
user_pref("dom.disable_window_move_resize", true);
// désactiver mise en avant ou en background par js 
user_pref("dom.disable_window_flip", true);
// désactiver voulez vous vraiment quitter la page par javascript
user_pref("dom.disable_beforeunload", true);
// désactiver la désactivation du bouton close sur une popup par javascript
user_pref("dom.disable_window_open_feature.close", true);
// désactiver la désactivation barre adresse sur une popup par javascript
user_pref("dom.disable_window_open_feature.location", true);
// désactiver la désactivation barre menu sur une popup par javascript
user_pref("dom.disable_window_open_feature.menubar", true);
// désactiver la désactivation minimisable sur une popup par javascript
user_pref("dom.disable_window_open_feature.minimizable", true);
// désactiver la désactivation de la barre personelle sur une popup par javascript
user_pref("dom.disable_window_open_feature.personalbar", true);
// désactiver la désactivation du redimensionnement sur une popup par javascript
user_pref("dom.disable_window_open_feature.resizable", true);
// désactiver la désactivation des ascenseurs sur une popup par javascript, encore valable?
user_pref("dom.disable_window_open_feature.scrollbars", true);
// désactiver la désactivation barre de statut sur une popup par javascript, encore valable?
user_pref("dom.disable_window_open_feature.status", true);
// désactiver la désactivation barre de titre sur une popup par javascript, encore valable?
user_pref("dom.disable_window_open_feature.titlebar", true);
// désactiver la désactivation barre d'outils sur une popup par javascript, encore valable?
user_pref("dom.disable_window_open_feature.toolbar", true);
// désactiver le changement de statut par javascript, encore valable?
user_pref("dom.disable_window_status_change", true);

//performances réseaux, commenter si pbs
user_pref("network.http.max-connections-per-server", 16);
user_pref("network.http.max-persistent-connections-per-server", 16);
user_pref("network.http.pipelining", true);
user_pref("network.http.pipelining.maxrequests", 10);
user_pref("network.http.proxy.pipelining", true);

/////////////  pour info
// désactiver openh264, plugin for webRtc H264
//pref("media.gmp-gmpopenh264.enabled", false);
// désactiver l'animation des images gif, par défaut normal
// user_pref("image.animation_mode", "none");
// cache disk en mémoire et pas sur disk, défaut true, utile sur SSD et/ou grosse mémoire vive
// user_pref("browser.cache.disk.enable", "false");
// vérifier
// user_pref("browser.cache.memory.enable", "true");
// auparavant, options toujours valide? valeur en octets:
// user_pref("browser.cache.memory.capacity", "128000");

//////////// A tester avant adoption par défaut
// send a referer header with the target URI as the source, défaut false, à tester
// user_pref("network.http.referer.spoofSource", false);
// force à réutiliser bookmarks.html, défaut false, à tester
// user_pref("browser.bookmarks.autoExportHTML", false);
