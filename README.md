# getFirefox  


> **avertissement**   
> la nouvelle version ESR ( version 60.x ), basée sur Quantum, **empêche tout retour en arrière**

* les extensions incompatibles avec WebExtension seront désacivées, malheureusement les plus pointues n'ont pas d'équivalent dans à cause des limitations de WebExtension qui se cantonne à essayer de suivre les préconisation de Google 
* gestion des utilisateurs différente
* il faut un processeur ayant le flag cpu sse2 pour faire fonctionner firefox (depuis la version 53). Pour éviter tout probème, utiliser des processeurs pas plus anciens que les p4 ou au athlon64 (ou processeurs de x86 de la même époque)

> Ceux qui ne croient pas en l'avenir marketé _fabuleux_ de Quantum (publicité intégrée à venir, webextension limitées chez Mozilla, etc), qui présage plutôt un suivi sans saveur de Google Chrome, en attendant son abandon après une longue agonie déjà amorcée depuis longtemps, peuvent [tester Waterfox](https://framaclic.org/h/doc-getxfox)  
> Waterfox est un fork basé sur l'ancien moteur avec tout les extensions fonctionnelles et une bien plus grande liberté de personnalisation

![version: 4.20.1](https://img.shields.io/badge/version-4.20.1-blue.svg?longCache=true&style=for-the-badge)
![bash langage](https://img.shields.io/badge/bash-4-brightgreen.svg?longCache=true&style=for-the-badge)
![license LPRAB / WTFPL](https://img.shields.io/badge/license-LPRAB%20%2F%20WTFPL-blue.svg?longCache=true&style=for-the-badge)

> c'est un script bash qui télécharge les dernières versions officielles de Mozilla Firefox 
  des différents canaux possibles: **latest** la release officielle, mais aussi **ESR** **beta**, **nightly**, 
  et _dev_.  
> les versions peuvent coexister et fonctionner en parallèle  
> les mises à jour de Firefox sont par défaut autorisées et gérées par Firefox (corrections mineures   
  permanentes, en tâche de fond, et une version majeure toutes les 6 semaines environ).  
> le script se mettra éventuellement à jour, sans influence sur les canaux Firefox installés.  
> le script installe un canal Firefox pour l'utilisateur en cours.   
> le script peut charger des fichiers de personnalisation et les mettra à jour périodiquement  
> le script peut désinstaller les canaux Firefox souhaités  
> script testé sur debian / ubuntu, mais devrait être compatible avec d'autres distributions  


* les installations/désinstallations/opérations système doivent être faites avec les privilèges **root**
* la mise à jour du script ou autres opérations légères peuvent être faites en utilisateur.


> le programme Tor Browser est dorénavant pris en charge par [getXfox](https://framaclic.org/h/doc-getxfox)


## installation rapide du script

* privilèges **root** requis

```shell
wget -O getFirefox https://framaclic.org/h/getfirefox
chmod +x getFirefox && ./getFirefox
```
```text
              _   _____ _           __ 
    __ _  ___| |_|  ___(_)_ __ ___ / _| _____  __ 
   / _' |/ _ \ __| |_  | | '__/ _ \ |_ / _ \ \/ / 
  | (_| |  __/ |_|  _| | | | |  __/  _| (_) >  <  
   \__, |\___|\__|_|   |_|_|  \___|_|  \___/_/\_\ 
   |___/  version 4.22.0 - 12/06/2018
 
  getFirefox 4.22.0 installé dans le système.
  maintenant, appel du script par: getFirefox (sans ./)

```

* le script est maintenant dans le système et tout utilisateur peut s'en servir.
* **Firefox n'est pas encore installé**
* un canal Firefox peut maintenant être choisi et installé (`latest`, `esr`, `beta`, `nightly`, `dev`)


## installation d'un canal Firefox

```shell
getFirefox i-canal
```
* privilèges **root** requis
* `getFirefox p-all` possible
* la version stable en cours de Firefox est nommée **latest**

```text
              _   _____ _           __ 
    __ _  ___| |_|  ___(_)_ __ ___ / _| _____  __ 
   / _' |/ _ \ __| |_  | | '__/ _ \ |_ / _ \ \/ / 
  | (_| |  __/ |_|  _| | | | |  __/  _| (_) >  <  
   \__, |\___|\__|_|   |_|_|  \___|_|  \___/_/\_\ 
   |___/  version 4.22.0 - 12/06/2018

  installation Firefox-nightly

    - téléchargement...

/tmp/getFirefox-install_ffx/firefox-62.0a1_night 100%[============================>]  59,86M  5,66MB/s    ds 12s     

   - décompression...

   - installation...

  profil Firefox nightly configuré
  Firefox nightly est le défaut système

  Firefox-nightly 62.0a1 installé

```

* la dernière version officielle du canal Firefox est installée, en étant directement chargée sur le site Mozilla
* un lanceur par canal installé est placé dans les menus (Applications/Internet)
* l'installation de Firefox sur un canal existant est refaite en **totalité**, mais **le profil 
  n'est pas modifié**
* chaque canal peut être lancé en console: `firefox-latest` `firefox-beta` `firefox-dev` `firefox-nightly` 
  `firefox-esr`
* le dernier canal installé est configuré comme défaut dans le système, c'est-à-dire comme:
   * cible de la commande `firefox` en console (en plus de firefox-canal) 
   * prioritaire dans update-alternatives (si supporté)
   * par défaut dans le profiles.ini de Thunderbird
* pour chaque canal, une option permet basculer cette priorité sur le canal choisi
* les canaux par ordre croissant de versions:   
`    ESR  <= release < beta = developer edition < nightly    ` 
* le canal **all** est fictif et comprend _esr+latest+beta+nightly_ pour l'installation (pas de _dev_ donc)
* le canal **all** comprend toutes les versions installées, pour les autres opérations
* pour chaque canal, une option permet de **copier** un éventuel profil _.default_. 
  le profil _.default_ existant est laissé en place.

4 autres canaux, en dehors de latest: 

 * Beta, qui deviendra la future version Release
 * Nightly, réservé aux aventuriers, mise à jour tous les jours 
   [Mozilla Nightly](https://blog.nightly.mozilla.org/) 
 * ESR, mise à jour majeure toutes les ans environ, avec support à plus long terme. 
   en savoir plus [Mozilla ESR](https://developer.mozilla.org/fr/Firefox/Firefox_ESR) 
 * Developer Edition, basée sur la beta (avec des outils de développement web?)



## help

```shell
getFirefox -h
```
```text
              _   _____ _           __ 
    __ _  ___| |_|  ___(_)_ __ ___ / _| _____  __ 
   / _' |/ _ \ __| |_  | | '__/ _ \ |_ / _ \ \/ / 
  | (_| |  __/ |_|  _| | | | |  __/  _| (_) >  <  
   \__, |\___|\__|_|   |_|_|  \___|_|  \___/_/\_\ 
   |___/  version 4.22.0 - 12/06/2018

      canaux possibles: latest, beta, nightly, dev, esr ( <all> = tous les canaux sauf dev )
  
  exemple, installation version Release (latest): getFirefox i-latest
  ----------------------------------------------------------------------
  getFirefox i-canal       : installation de Firefox <canal> (root)
  getFirefox d-canal       : copier un éventuel profil .default existant sur <canal>
  getFirefox m-canal archi : installation sur le <canal> d'une <archi>ve téléchargée manuellement (root)
  getFirefox r-canal       : désinstallation (remove) du <canal> (root)
  getFirefox ri            : réparation icône(s) et lanceur(s) dans le menu (root)
  getFirefox t-canal       : téléchargement du <canal> dans le répertoire courant (sans installation)
  getFirefox u-canal       : profil pour l'utilisateur en cours et comme défaut système (root)

  getFirefox p-canal       : personnalisation sur le <canal> de user.js & userChrome.css
  getFirefox pr-canal      : suppression des personnalisations (remove) sur le <canal>
  getFirefox pu            : mise à jour des personnalisations (update) installées

  getFirefox version       : versions installées et en ligne
  
    --dev   : une version de dev du script (si existante) est recherchée
    --sauve : le téléchargement est sauvegardé dans le répertoire courant en plus de l'installation
  ----------------------------------------------------------------------
  ./getFirefox (ou ./getFirefox -i) : installation du script dans le système (root)
  getFirefox -h, --help    : affichage aide
  getFirefox -r, --remove  : désinstallation du script (root)
  getFirefox -u, --upgrade : mise à jour du script
  getFirefox -v, --version : version du script

  plus d'infos: https://framaclic.org/h/doc-getfirefox
  Tor Browser, voir getXfox: https://framaclic.org/h/doc-getxfox

```


## version

```shell
getFirefox version
```
```text
              _   _____ _           __ 
    __ _  ___| |_|  ___(_)_ __ ___ / _| _____  __ 
   / _' |/ _ \ __| |_  | | '__/ _ \ |_ / _ \ \/ / 
  | (_| |  __/ |_|  _| | | | |  __/  _| (_) >  <  
   \__, |\___|\__|_|   |_|_|  \___|_|  \___/_/\_\ 
   |___/  version 4.22.0 - 12/06/2018

  script en place: 4.22.0
  script en ligne: 4.22.0

  .....++++

  Firefox en place: esr: 52.6.0                        beta: 61.0     nightly: 62.0a1
  Firefox en ligne: esr: 60.0.2    latest: 60.0.2      beta: 61.0b12  nightly: 62.0a1   esr_old: 52.8.1

  perso. Firefox en place: esr: 0.6  beta: 0.6
  perso. Firefox en ligne: 0.6

```


## mise à jour Firefox

* Firefox gère ses mises à jour et le script permet cet automatisme.
* cette mise à jour se fait en tâche de fond et est disponible au prochain redémarrage
* la mise à jour éventuelle peut être déclenchée manuellement avec le menu `Aide/A propos de Firefox`
* les correctifs mineurs sont appliqués selon leur disponibilité
* mise à jour majeure toutes les 6 à 8 semaines environ, la version beta descend en version Release,
  la version nightly, figée, descend en beta
* la nightly est mise à jour quotidiennement, voir plus
* si Firefox n'est pas utilisé, il ne se met pas à jour


## profil default, copie

```shell
getFirefox d-canal
```

* `getFirefox d-all` possible
* duplique un éventuel profil .default existant (paquet distribution par exemple) sur un canal choisi.


## nouvel utilisateur ou reconfiguration profil

```shell
getFirefox u-canal
```

* privilèges **root** requis
* `getFirefox d-all` possible (plus haut canal comme défaut système)
* ajoute un profil pour un canal Firefox **installé**, pour l'utilisateur en cours
* configure le canal comme navigateur par **défaut** (alternatives, commande firefox, profil Firefox)
* évite de télécharger inutilement une nouvelle fois pour un nouvel utilisateur
* pour ajouter un autre utilisateur, titi par exemple: `USER_INSTALL=titi getFirefox u-canal`, ça devrait 
  marcher (pas testé)


## personnalisation (installation)

```shell
getFirefox p-canal
```

* `getFirefox p-canal` possible
* _user.js_ est ajouté dans le profil du canal souhaité
* _userChrome.css_ est ajouté dans le profil du canal souhaité, sous-répertoire _chrome/_
* la version indiquée dans user.js fait référence, la version de userChrome.css est indicative
* les personnalisations sont périodiquement mise à jour, comme le script (7jours)
* l'url de base peut être changée aisément, les fichiers peuvent être modifiés et hébergés ailleurs pour mise à jour 
  automatique (sur une plateforme git ou un snippet par exemple)
* **LIMITATION** une seule url de mise à jour pour tous canaux. à voir si intérêt pour changer cela
* la première url personnalisée trouvée est sélectionnée, dans l'ordre: esr latest beta nightly dev 
* dans chaque canal concerné, la configuration se trouve trouve dans le fichier: 
  `/home/user/.mozilla/firefox/canal/personnalisation`
   * l'url (**première ligne**) peut y être modifiée
   * la version est indiquée en **seconde ligne**
* pour figer des fichiers de personnalisations et ne pas les mettre à jour, il suffit d'effacer les fichiers
  `personnalisation`

* contenu de [user.js](https://framagit.org/sdeb/getFirefox/blob/master/user.js)
* contenu de [userChrome.css](https://framagit.org/sdeb/getFirefox/blob/master/userChrome.css)


## personnalisation (upgrade)

```shell
getFirefox pu
```

* cette tâche est exécutée périodiquement par cron/anachron et n'a pas vraiment vocation à être lancée 
  manuellement
* toutes les personnalisations installées sont mise à jour


## personnalisation (suppression)

```shell
getFirefox pr-canal
```

* `getFirefox p-canal` possible


## désinstallation d'un canal Firefox

```shell
getFirefox r-canal
```

* privilèges **root** requis
* `getFirefox p-canal` possible
* le profil pour firefox **n'est pas supprimé**, il sera donc utilisable en cas de réinstallation
* si firefox-canal est ouvert, il sera fermé
* le navigateur par **défaut** sera configuré sur le plus bas canal encore installé



## installation manuelle d'une archive

```shell
getFirefox m-canal firefox-VERSION.fr.linux-ARCHITECTURE.tar.bz2
```

* privilèges **root** requis
* installe une archive téléchargée manuellement   


## suppression d'un profil Firefox

* **FERMER toutes les instances ouvertes** du canal
* **en user**, lancer en terminal le profil manager de Firefox: `firefox -P` :
* sélectionner le profil souhaité
* cliquer sur _supprimer un profil_
     * _supprimer les fichiers_, cela supprimera aussi le répertoire `~/.mozilla/firefox/profilSélectionné`   
       le profil est **définitivement** détruit
* quitter


### suppression manuelle de tous les profils

si plus aucun canal de Firefox n'est installé, qu'il n'y a plus de profil manager disponible, et que vous
êtes certains de vouloir supprimer tous les profils en place, en **user**:

```shell
rm -rf ~/.mozilla/firefox/
```


## mise à jour script & personnalisation

```shell
getFirefox -u
```

* test toutes les **semaines**
* mise à jour du **script** si une nouvelle version est disponible en ligne. cela n'influe pas sur les
  canaux Firefox installés.
* cette tâche est exécutée périodiquement par cron/anachron et n'a pas vraiment vocation à être lancée manuellement
* _anacron_ est utilisé, c'est à dire que la mise à jour sera testée, dès le redémarrage du Pc
* si une **personnalisation** est mise en place, une mise à jour possible sera elle aussi testée, pour tous les  
  canaux concernés


## logs

```shell
pager /var/log/sdeb_getFirefox.log
```

tous les évènements importants sont consignés dans le fichier _/var/log/sdeb_getFirefox.log_   


## supprimer le script

```shell
getFirefox -r
```

* privilèges **root** requis
* effacement du script dans le système (_/opt/bin_)
* effacement de l'inscription dans crontab/anacron utilisateur
* cela ne **supprime pas** les éventuels canaux Firefox installés


## sources

sur [framagit](https://framagit.org/sdeb/getFirefox/blob/master/getFirefox)


## changelog

sur [framagit](https://framagit.org/sdeb/getFirefox/blob/master/CHANGELOG.md)


## contact

pour tout problème ou suggestion concernant ce script, n'hésitez pas à ouvrir une issue 
[Framagit](https://framagit.org/sdeb/getFirefox/issues)

IRC: ##sdeb@freenode.net


## license

[LPRAB/WTFPL](https://framagit.org/sdeb/getFirefox/blob/master/LICENSE.md)


![compteur](https://framaclic.org/h/getfirefox-gif)
